slides presentation
--------------------

Maintainers:
 Marouan Hammami mh.marouan@gmail.com / https://www.drupal.org/user/3203501

SUMMARY:

This module is for easily create a multistep form and depends on the <a href="https://www.drupal.org/project/field_group">Field group</a> module.

<strong>Usage</strong>

<ol>
  <li>Go to "Manage form display" for the content</li>
  <li>Add group for each step with type "Multistep Submit"</li>
  <li>Move fields for each group</li>
  <li>Save</li>
</ol>

<strong>Installation</strong>
<ol>
  <li>Install <a href="https://www.drupal.org/project/libraries"> Libraries API 2.x</a></li>
  <li>Download <a href="https://github.com/rstaib/jquery-steps">jQuery Steps Plugin</a> </li>
  <li>Put the folder in a libraries directory  /libraries/jquery-steps</li>
  <li>Install and enable this module</li>
</ol>

<strong>Settings jQuery Steps</strong> 

Configure Settings JQuery Steps via admin/multistep/settings



